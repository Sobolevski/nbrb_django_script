from django.db import models


class Currencies(models.Model):
    name = models.CharField(max_length=40, unique=True)
    name_ru = models.CharField(max_length=40)
    scale = models.IntegerField()
    abbreviation = models.CharField(max_length=3)

    def __str__(self):
        return self.name


class ExchangeRates(models.Model):
    date = models.DateField()
    rate = models.FloatField()
    currency = models.ForeignKey(Currencies, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.date, self.currency.name)


class InfoCurrencies(models.Model):
    abbreviation = models.CharField(max_length=4)
    info = models.TextField()
    info_ru = models.TextField()
    currency = models.ForeignKey(Currencies, on_delete=models.CASCADE)

