import datetime
from .models import ExchangeRates, InfoCurrencies
from django.shortcuts import render
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.conf import settings

TODAY = datetime.date.today()

NUMBER_OF_CURRENCY = 26

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


#@cache_page(CACHE_TTL)
def exchange_rates(request):
    daily_exrates = ExchangeRates.objects.order_by('-pk')[:NUMBER_OF_CURRENCY]
    yesterday_exrates = ExchangeRates.objects.order_by('-pk')[NUMBER_OF_CURRENCY:NUMBER_OF_CURRENCY * 2]
    data = {
        'daily_exrates': zip(daily_exrates, yesterday_exrates),
        'date': daily_exrates[0].date
    }
    return render(request, 'currencies/exrates.html', data)


#@cache_page(CACHE_TTL)
def info_currency(request, abbreviation):
    info_currency = InfoCurrencies.objects.filter(abbreviation=abbreviation)
    context = {
        'info_currency': info_currency,
    }
    return render(request, 'currencies/info_currency.html', context)
