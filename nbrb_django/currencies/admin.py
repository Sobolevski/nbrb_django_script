from django.contrib import admin
from .models import ExchangeRates, InfoCurrencies, Currencies

admin.site.register(Currencies)
admin.site.register(ExchangeRates)
admin.site.register(InfoCurrencies)
