from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'/info/(?P<abbreviation>[A-Z]{3})', views.info_currency, name='info_currency'),
    url(r'/', views.exchange_rates, name='exrates'),
]