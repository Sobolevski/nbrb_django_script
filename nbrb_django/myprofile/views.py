from django.shortcuts import redirect, render


def profile(request):
    if request.user.is_authenticated:
        return render(request, 'myprofile/profile.html')
    else:
        return redirect('/')