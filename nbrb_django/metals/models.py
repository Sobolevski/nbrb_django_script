from django.db import models


class Metals(models.Model):
    name = models.CharField(max_length=15, unique=True)
    name_ru = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class IngotRates(models.Model):
    date = models.DateField()
    value = models.FloatField()
    metal = models.ForeignKey(Metals, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.date, self.metal.name)


class BpsRates(models.Model):
    date = models.DateField()
    value_sale = models.FloatField()
    value_buy = models.FloatField()
    metal = models.ForeignKey(Metals, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.date, self.metal.name)


class InfoMetal(models.Model):
    info = models.TextField()
    info_ru = models.TextField()
    metal = models.ForeignKey(Metals, on_delete=models.CASCADE)

    def __str__(self):
        return self.metal.name
