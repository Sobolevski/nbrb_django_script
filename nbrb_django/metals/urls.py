from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'/info/(?P<name>.+)', views.info_metal, name='info_metal'),
    url(r'/', views.price_metals, name='ingot_rates')
]
