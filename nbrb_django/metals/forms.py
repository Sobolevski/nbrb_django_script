from django import forms
import datetime


TODAY = datetime.datetime.today()
START_DAY = (TODAY - datetime.timedelta(days=(TODAY.day - 1))).strftime('%Y-%m-%d')
END_DATE = TODAY.strftime('%Y-%m-%d')


class DateForm(forms.Form):
    start_date = forms.DateField(initial=START_DAY, label='Дата начала периода', widget=forms.TextInput(attrs={'class': 'form-control'}))
    end_date = forms.DateField(initial=END_DATE, label='Дата конца периода', widget=forms.TextInput(attrs={'class': 'form-control'}))
