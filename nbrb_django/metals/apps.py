from django.apps import AppConfig


class MetalsConfig(AppConfig):
    name = 'metals'
