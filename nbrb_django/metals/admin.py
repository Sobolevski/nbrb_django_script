from django.contrib import admin
from .models import Metals, InfoMetal, IngotRates, BpsRates

admin.site.register(Metals)
admin.site.register(IngotRates)
admin.site.register(InfoMetal)
admin.site.register(BpsRates)