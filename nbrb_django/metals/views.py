import datetime
from django.shortcuts import render
from .models import IngotRates, InfoMetal, Metals, BpsRates
from .forms import DateForm
from django.utils.translation import ugettext as _
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.conf import settings


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

TODAY = datetime.date.today()

NUMBER_OF_METALS = 4


#@cache_page(CACHE_TTL)
def price_metals(request):
    daily_price_ingots = IngotRates.objects.order_by('-pk')[:NUMBER_OF_METALS]
    yesterday_price_ingots = IngotRates.objects.order_by('-pk')[NUMBER_OF_METALS:NUMBER_OF_METALS * 2]
    today_bps_price = BpsRates.objects.order_by('-pk')[:NUMBER_OF_METALS]
    context = {
        'metals_price': zip(daily_price_ingots, yesterday_price_ingots, today_bps_price),
        'date_nbrb': daily_price_ingots[0].date,
        'date_bps': today_bps_price[0].date
    }
    return render(request, 'metals/price_metals.html', context)


#cache_page(CACHE_TTL)
def info_metal(request, name):
    metal = Metals.objects.get(name=name)
    info_metal = InfoMetal.objects.filter(metal=metal)
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            list_daily_price_ingots = IngotRates.objects.filter(metal=metal)
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            list_of_metal = []
            for ingot in list_daily_price_ingots:
                if ingot.date >= start_date and ingot.date <= end_date:
                    list_of_metal.append(ingot)
            context = {
                'list_of_metal': list_of_metal,
                'forms': form,
                'info_metal': info_metal
            }
            if list_of_metal == []:
                context['error'] = _('Date input error, try again.')
            return render(request, 'metals/info_metal.html', context)

    else:
        form = DateForm()

        context = {
            'text': _('Enter the date range for which you want to display prices'),
            'forms': form,
            'info_metal': info_metal
        }
        return render(request, 'metals/info_metal.html', context)
