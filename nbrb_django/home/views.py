from django.shortcuts import render
from django.utils.translation import ugettext as _


def index(request):
    info = _('''This application updates daily exchange rates and prices for precious metals 
    of the National Bank of the Republic of Belarus. 
    The application is written using python3 and the django web framework.''')
    return render(request, 'home/index.html', {'info': info})


