from django.contrib import admin
from .models import NotificationsMetals, NotificationsCurrencies

admin.site.register(NotificationsMetals)
admin.site.register(NotificationsCurrencies)
