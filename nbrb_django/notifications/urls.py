from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'/delete-metal/(?P<pk>\d+)', views.delete_metal, name='delete_metal'),
    url(r'/delete-currency/(?P<pk>\d+)', views.delete_сurrency, name='delete_currency'),
    url(r'/', views.notifications)
]
