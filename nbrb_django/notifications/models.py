from django.db import models
from basket.models import ChooseMetal


class NotificationsMetals(models.Model):
    email = models.EmailField()
    sign = models.CharField(max_length=1)
    value = models.FloatField()
    locale = models.CharField(max_length=2)
    metal = models.ForeignKey('metals.Metals', on_delete=models.CASCADE)

    class Meta:
        unique_together = ("email", "sign", "metal")


class NotificationsCurrencies(models.Model):
    email = models.EmailField()
    sign = models.CharField(max_length=1)
    value = models.FloatField()
    locale = models.CharField(max_length=2)
    currency = models.ForeignKey('currencies.Currencies', on_delete=models.CASCADE)

    class Meta:
        unique_together = ("email", "sign", "currency")
