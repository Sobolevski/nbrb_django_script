from django.shortcuts import render, redirect
from .models import NotificationsCurrencies, NotificationsMetals
from metals.models import Metals
from currencies.models import Currencies
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext as _


def notifications(request):
    metals = Metals.objects.all()
    metals_list = list(metal.name for metal in metals)

    currencies = Currencies.objects.all()
    error = ''
    if request.POST:
        name = request.POST.get('dropdown-searchfield')
        sign = request.POST.get('sign')
        value = request.POST.get('value')
        try:
            assert int(value) >= 0
            if name not in metals_list:
                cur = Currencies.objects.get(name=name)
                currency = NotificationsCurrencies(email=request.user.email, sign=sign, value=value,
                                                   locale=request.LANGUAGE_CODE, currency=cur)
                currency.save()
            else:
                met = Metals.objects.get(name=name)
                metal = NotificationsMetals(email=request.user.email, sign=sign, value=value,
                                            locale=request.LANGUAGE_CODE, metal=met)
                metal.save()
        except IntegrityError:
            error = _('For each metal or currency, you can add only one comparison with the same condition.')
        except ValueError:
            error = _('Error input price')
        except AssertionError:
            error = _('Only positive value is possible')

    user_metals = NotificationsMetals.objects.filter(email=request.user.email)
    user_currencies = NotificationsCurrencies.objects.filter(email=request.user.email)

    data = {
        'metals': metals,
        'currencies': currencies,
        'user_metals': user_metals,
        'user_currencies': user_currencies,
        'error': error
    }

    return render(request, 'notifications/notifications.html', data)


def delete_сurrency(request, pk):
    try:
        currency = NotificationsCurrencies.objects.get(pk=pk)
        if currency.email == request.user.email:
            currency.delete()
        else:
            return render(request, 'error.html')
    except ObjectDoesNotExist:
        return render(request, 'error.html')
    return redirect('/notifications/')


def delete_metal(request, pk):
    try:
        metal = NotificationsMetals.objects.get(pk=pk)
        if metal.email == request.user.email:
            metal.delete()
        else:
            return render(request, 'error.html')
    except ObjectDoesNotExist:
        return render(request, 'error.html')
    return redirect('/notifications/')
