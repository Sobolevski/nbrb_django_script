from __future__ import absolute_import
import os
import django
import requests
import datetime
from celery.task.base import periodic_task
from bs4 import BeautifulSoup
from django.core.mail import send_mail
from django.db import IntegrityError

os.environ['DJANGO_SETTINGS_MODULE'] = 'nbrb_django.settings'
django.setup()

from notifications.models import NotificationsMetals, NotificationsCurrencies
from metals.models import Metals, IngotRates, BpsRates
from currencies.models import Currencies, ExchangeRates

URL_GET_PRICES = 'http://www.nbrb.by/API/BankIngots/Prices/'
URL_LIST_OF_METALS = 'http://www.nbrb.by/API/Metals'
URL_GET_EXCHANGE_RATES = 'http://www.nbrb.by/API/ExRates/Rates/'
URL_GET_EXCHANGE_INFO = 'http://www.nbrb.by/API/ExRates/Currencies/'
BPS_URL = 'http://www.bps-sberbank.by/43257f17004e948d/dm_rates?open&date='
BPS_NUMBER_OF_PRICES = 4
NUMBER_OF_CURRENCY = 26
TODAY = datetime.datetime.now().strftime('%Y-%m-%d')

DATE_FOR_REQ = {
    'startDate': TODAY,
    'endDate': TODAY
}

PERIODICITY = {
    'Periodicity': 0
}

SUBJECT = 'NBRB notifications'


@periodic_task(run_every=datetime.timedelta(days=1))
def every_day_task():

    def write_db_exchange_rates():
        r = requests.get(URL_GET_EXCHANGE_RATES, params=PERIODICITY)
        exchange_rates = r.json()
        yesterday_rates = ExchangeRates.objects.order_by('-pk').reverse()[NUMBER_OF_CURRENCY:NUMBER_OF_CURRENCY * 2]

        counter = 0
        for i in range(0, NUMBER_OF_CURRENCY):
            if exchange_rates[i]['Cur_OfficialRate'] == yesterday_rates[i].rate:
                counter += 1

        if counter != NUMBER_OF_CURRENCY:
            for unit in exchange_rates:
                req = requests.get(URL_GET_EXCHANGE_INFO + str(unit['Cur_ID']))
                name_en = req.json()['Cur_Name_Eng']
                name_ru = req.json()['Cur_Name']
                try:
                    currency = Currencies.objects.create(name=name_en, name_ru=name_ru, scale=unit['Cur_Scale'],
                                                         abbreviation=unit['Cur_Abbreviation'])
                except IntegrityError:
                    currency = Currencies.objects.get(name=name_en)
                    continue
                finally:
                    rate = ExchangeRates.objects.create(date=unit['Date'][:10], rate=unit['Cur_OfficialRate'],
                                                        currency=currency)

    def write_db_prices_metals():
        req = requests.get(URL_GET_PRICES, params=DATE_FOR_REQ)
        prices_for_metals = req.json()

        r = requests.get(URL_LIST_OF_METALS)
        list_of_metals = r.json()

        for unit in prices_for_metals:
            print(unit)
            try:
                metal = Metals.objects.create(name=list_of_metals[unit['MetalId']]['NameEng'],
                                              name_ru=list_of_metals[unit['MetalId']]['Name'])
            except IntegrityError:
                metal = Metals.objects.get(name=list_of_metals[unit['MetalId']]['NameEng'])
                continue
            finally:
                value = IngotRates.objects.create(date=unit['Date'][:10], value=unit['Value'], metal=metal)

    def write_db_prices_bps_metals(today=datetime.datetime.now()):
        met_dict = {
            'Золото': 'Gold',
            'Серебро': 'Silver',
            'Платина': 'Platinum',
            'Палладий': 'Palladium'
        }
        r = requests.get(BPS_URL + today.strftime("%Y.%m.%d"))

        soup = BeautifulSoup(r.text, 'html.parser')
        metals = soup.find_all("div", "curr_row")
        list_of_metals = [m.text.capitalize() for m in metals]
        soup_date = BeautifulSoup(r.text, 'html.parser')
        prices = soup_date.find_all("div", class_="td-text")
        price_of_metals = [price.text for price in prices]
        start = 0
        for metal in list_of_metals:
            price_of_metal = price_of_metals[start: start + BPS_NUMBER_OF_PRICES]
            start += BPS_NUMBER_OF_PRICES
            m = Metals.objects.get(name=met_dict[metal])
            value = BpsRates.objects.create(date=TODAY, value_sale=price_of_metal[3], value_buy=price_of_metal[1],
                                            metal=m)

    def metals_mail():
        metals_rates = IngotRates.objects.order_by('-pk')[:4]
        metals_price_dict = {}
        for metal in metals_rates:
            metals_price_dict[metal.metal.name] = metal.value
        notifications_metals = NotificationsMetals.objects.all()
        for notification in notifications_metals:
            if notification.sign == '>':
                if metals_price_dict[notification.metal.name] > notification.value:
                    if notification.locale == 'ru':
                        (send_mail(SUBJECT, 'Цена на металл "{}" достигла установленной отметки {} BYN. '
                                            'Теперь она составляет {} BYN.'.format(notification.metal.name_ru,
                                                                                   notification.value,
                                                                                   metals_price_dict[
                                                                                       notification.metal.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))
                    else:
                        (send_mail(SUBJECT, 'The price of metal {} has reached the mark {} BYN. '
                                            'Now it is {} BYN.'.format(notification.metal.name, notification.value,
                                                                       metals_price_dict[notification.metal.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))
            elif notification.sign == '<':
                if metals_price_dict[notification.metal.name] < notification.value:
                    if notification.locale == 'ru':
                        (send_mail(SUBJECT, 'Цена на металл "{}" опустилась установленной отметки {} BYN. '
                                            'Теперь она составляет {} BYN.'.format(notification.metal.name_ru,
                                                                                   notification.value,
                                                                                   metals_price_dict[
                                                                                       notification.metal.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))
                    else:
                        (send_mail(SUBJECT, 'The price of metal {} fell below the set mark {} BYN. '
                                            'Now it is {} BYN.'.format(notification.metal.name, notification.value,
                                                                       metals_price_dict[notification.metal.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))

    def currencies_mail():
        exchange_rates = ExchangeRates.objects.order_by('-pk')[:26]
        exrates_dict = {}
        for rate in exchange_rates:
            exrates_dict[rate.currency.name] = rate.rate
        notifications_currencies = NotificationsCurrencies.objects.all()
        for notification in notifications_currencies:
            if notification.sign == '>':
                if exrates_dict[notification.currency.name] > notification.value:
                    if notification.locale == 'ru':
                        (send_mail(SUBJECT, 'Цена на валюту "{}" достигла установленной отметки {} BYN. '
                                            'Теперь она составляет {} BYN.'.format(notification.currency.name_ru,
                                                                                   notification.value,
                                                                                   exrates_dict[
                                                                                       notification.currency.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))
                    else:
                        (send_mail(SUBJECT, 'The price of the currency "{}" has reached the mark {} BYN. '
                                            'Now it is {} BYN.'.format(notification.currency.name_ru,
                                                                       notification.value,
                                                                       exrates_dict[notification.currency.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))
            if notification.sign == '<':
                if exrates_dict[notification.currency.name] < notification.value:
                    if notification.locale == 'ru':
                        (send_mail(SUBJECT, 'Цена на валюту "{}" опустилась ниже установленной отметки {} BYN. '
                                            'Теперь она составляет {} BYN.'.format(notification.currency.name_ru,
                                                                                   notification.value,
                                                                                   exrates_dict[
                                                                                       notification.currency.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))
                    else:
                        (send_mail(SUBJECT,
                                   'The price of the currency "{}" fell below the set mark {} BYN. Now it is {} BYN.'.format(
                                       notification.currency.name_ru, notification.value,
                                       exrates_dict[notification.currency.name]),
                                   'nbrb.django@gmail.com', [notification.email], fail_silently=False))

    write_db_exchange_rates()
    write_db_prices_metals()
    write_db_prices_bps_metals()
    #metals_mail()
    #currencies_mail()


every_day_task()