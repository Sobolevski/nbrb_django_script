$(document).ready(function(){
        $('select[name="dropdown"], select[name="dropdown-optgroup"], select[name="dropdown-disabled"]').select2({dropdownCssClass: 'select-inverse-dropdown'});

        $('select[name="searchfield"]').select2({dropdownCssClass: 'show-select-search'});
        $('select[name="dropdown-searchfield"]').select2({dropdownCssClass: 'select-dropdown show-select-search'});
      });