from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from metals.models import IngotRates, InfoMetal, Metals
from django.db import IntegrityError
from basket.models import ChooseMetal, ChooseCurrency
from currencies.models import ExchangeRates, Currencies


def basket(request):
    if request.user.is_authenticated:
        user_currency = ChooseCurrency.objects.filter(username=request.user)
        user_metals = ChooseMetal.objects.filter(username=request.user)
        choosen_curr = list(curr.currency for curr in user_currency)
        choosen_metals = list(met.metal for met in user_metals)
        daily_exrates = ExchangeRates.objects.filter(currency__in=choosen_curr).order_by('-pk')[:len(choosen_curr)]
        daily_price_ingots = IngotRates.objects.filter(metal__in=choosen_metals).order_by('-pk')[:len(choosen_metals)]
        data = {
            'daily_exrates': daily_exrates,
            'daily_price_ingots': daily_price_ingots
        }
        if len(choosen_curr) != 0:
            data['date_rates'] = daily_exrates[0].date
        if len(daily_price_ingots) != 0:
            data['date_metal'] = daily_price_ingots[0].date
        return render(request, 'basket/basket.html', data)
    else:
        redirect('/')


def settings(request):
    if request.user.is_authenticated:
        data = {}
        data.update(csrf(request))
        metals_choose_user = ChooseMetal.objects.filter(username=request.user)
        metals_user = list(metal_user.metal for metal_user in metals_choose_user)
        currency_choose_user = ChooseCurrency.objects.filter(username=request.user).only('currency')
        currency_user = list(currencies_user.currency for currencies_user in currency_choose_user)

        if request.POST:
            metals = request.POST.getlist('metals')
            currencies = request.POST.getlist('currencies')
            for metal in metals:
                try:
                    met = Metals.objects.get(name=metal)
                    ingot = ChooseMetal(username=request.user, metal=met)
                    ingot.save()
                except IntegrityError:
                    continue
            for metal in metals_user:
                if metal.name not in metals:
                    met = Metals.objects.get(name=metal)
                    ingot = ChooseMetal.objects.get(username=request.user, metal=met)
                    ingot.delete()

            for currency in currencies:
                try:
                    cur = Currencies.objects.get(name=currency)
                    value = ChooseCurrency(username=request.user, currency=cur)
                    value.save()
                except IntegrityError:
                    continue
            for currency in currency_user:
                if currency.name not in currencies:
                    curr = ChooseCurrency.objects.get(username=request.user, currency=currency)
                    curr.delete()

            metals_choose_user = ChooseMetal.objects.filter(username=request.user)
            currency_choose_user = ChooseCurrency.objects.filter(username=request.user)

        metals = Metals.objects.all()
        currencies = Currencies.objects.all()
        data = {
            'list_of_metals': metals,
            'currencies': currencies,
            'metals_choose_user': metals_choose_user,
            'currency_choose_user': currency_choose_user
        }
        return render(request, 'basket/settings.html', data)
    else:
        return redirect('/')

