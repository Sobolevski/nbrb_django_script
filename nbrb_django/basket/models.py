from django.db import models
from metals.models import InfoMetal, Metals
from currencies.models import ExchangeRates


class ChooseMetal(models.Model):
    username = models.CharField(max_length=150)
    metal = models.ForeignKey('metals.Metals', on_delete=models.CASCADE)

    class Meta:
        unique_together = ("username", "metal")


class ChooseCurrency(models.Model):
    username = models.CharField(max_length=150)
    currency = models.ForeignKey('currencies.Currencies', on_delete=models.CASCADE)

    class Meta:
        unique_together = ("username", "currency")
