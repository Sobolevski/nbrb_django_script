from django.contrib import admin
from .models import ChooseMetal, ChooseCurrency

admin.site.register(ChooseMetal)
admin.site.register(ChooseCurrency)
